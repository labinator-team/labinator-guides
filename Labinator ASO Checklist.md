# Labinator ASO Checklist

There’re nearly 3 million apps in major app stores and it is needless to say that how hard it is to be found in this data ocean. Either you spend huge budgets to increase your app’s visibility like people did in the past, or you make ASO to increase your downloads organically.

## Keywords

These are the words which users (potential customers) use to find your app. There are a few things you should consider when choosing the right ones:

- While Google Play doesn’t provide a keyword field, you can use a maximum of 100 characters of keywords in the iOS App Store.
- Find relevant keywords to your app: You can use a thesaurus, social media and user reviews for that.
- Choose highest search score and less competitive keywords.
- Look at your competitors’ titles that rank higher than you and gain some understanding about how to rank higher.

## App Name

- Make it memorable, unique and fitting for your app. Consider adding relevant keywords you find to rank higher in the app store’s searches.
- Use a maximum of 30 characters in Google Play and a maximum of 255 characters in the iOS App Store for your app name.
- Stay away from keyword spamming.

## App Description

- It must be a maximum of 4000 characters in both IOS App Store & Google Play.
- The first 3 lines (255 characters) of your description are really really important!
- Google Play has an 80 character short description area.
- Add the evidence of success (number of downloads, good ratings etc) to your description.
- Adding social media accounts to your description is a good way to keep in touch with your audience.
- Don’t lie to users or manipulate them to download your app.
- Pay attention to grammatical errors and typos.
- Don’t be too technical.

## Screenshots & App Icon

- Ensure they are informative about your app.
- Each screenshot should show the specific benefit of the app.
- You should use supplemental text.
- iOS App Store allows 5, while Google Play allows 8 screenshots.
- App icon needs to be eye-catching, well-designed and most importantly it should explain what the app is about

## Preview Video

- Focus on the key features of your app and give users a realistic idea of your app’s experience
- You can put 30 sec. length video in the iOS App Store and 2 min length video in Google Play.

## Localization

- If you want to target the other markets and rank high in searches, you should present your app to users in their language. Localization is a great way to increase your potential audience and the number of downloads so you should translate keywords, description, screenshots and video for each market you targeted.

## Update Regularly

- Track your ASO performance weekly and take necessary actions
- Test your title, chosen keywords and change the bad performing keywords with new ones.
- Make seasonal updates for your app icon, screenshots and preview video.

## Consider Hiring An Expert

Our app marketing experts at Labinator are available at your service. Check the links below for more information.

- Complete App Store Optimization (ASO) Service: https://labinator.com/online-services/google-play-aso/
- Android Incentive Advertising: https://labinator.com/online-services/buy-app-downloads-android-app-marketing/
- Complete All-In-One Android App Marketing: https://labinator.com/online-services/complete-android-app-marketing/
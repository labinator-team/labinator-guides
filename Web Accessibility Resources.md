# Web Accessibility Resources

## Accessibility Standards

**WCAG 2.1 Guidelines**
[https://www.w3.org/TR/WCAG21](https://www.w3.org/TR/WCAG21)
Web Content Accessibility Guidelines WCAG 2.1.

**How to Meet WCAG?**
[https://www.w3.org/WAI/WCAG21/quickref/?versions=2.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.1)
A quick reference to Web Content Accessibility Guidelines WCAG 2.1 requirements.

**Layman's Version of WCAG 2.1**
[http://romeo.elsevier.com/accessibility\_checklist/](http://romeo.elsevier.com/accessibility_checklist/)
Layman's Version of WCAG 2.1 for testers, designers and developers.

**WCAG 2.1 at a glance**
[https://www.w3.org/WAI/standards-guidelines/wcag/glance/](https://www.w3.org/WAI/standards-guidelines/wcag/glance/)
Paraphrased summary of Web Content Accessibility Guidelines WCAG 2.1.

**Americans with Disabilities Act (ADA)**
[https://www.ada.gov/](https://www.ada.gov/)
Americans with Disabilities Act (ADA) regulations for businesses and State and local governments.

**Section 508**
[https://www.section508.gov/](https://www.section508.gov/)
Guidance to Federal agency staff who play a role in IT accessibility.

**WCAG Standards & Legislation**
[https://www.powermapper.com/blog/government-accessibility-standards/](https://www.powermapper.com/blog/government-accessibility-standards/)
Comparison of accessibility standards and legislation across 11 countries.

**Website Accessibility & the Law**
[https://www.searchenginejournal.com/website-accessibility-law/285199/](https://www.searchenginejournal.com/website-accessibility-law/285199/)
Website accessibility and the law, why your website must be accessible.

**EU Web Accessibility Directive**
[https://ec.europa.eu/digital-single-market/en/web-accessibility](https://ec.europa.eu/digital-single-market/en/web-accessibility)
EU members must monitor and report on the accessibility of the websites and mobile apps.

**W3C WAI Policies**
[https://www.w3.org/WAI/policies/](https://www.w3.org/WAI/policies/)
Overview of web accessibility laws and policies in 40 countries.


## Accessibility Guidelines & Checklists

**Accessibility for Teams**
[https://accessibility.digital.gov/](https://accessibility.digital.gov/)
Digital.gov guide for embedding accessibility and inclusive design practices into teams.

**WCAG 2.1 Quick Reference**
[http://3pha.com/wcag2/](http://3pha.com/wcag2/)
WCAG 2.1 interactive checklist.

**18F Accessibility Checklist**
[https://accessibility.18f.gov/checklist/](https://accessibility.18f.gov/checklist/)
18f.gov accessibility checklist.

**A11Y checklist**
[http://a11yproject.com/checklist.html](http://a11yproject.com/checklist.html)
A11Y's beginner guide to web accessibility.

**Vox Media**
[http://accessibility.voxmedia.com/](http://accessibility.voxmedia.com/)
Accessibility checklists for project managers, editorial teams, web developers and designers.

**Google Accessibility**
[https://www.google.com/accessibility/](https://www.google.com/accessibility/)
Google's web accessibility guides and resources.

**Make WordPress Accessible**
[https://make.wordpress.org/accessibility/handbook/](https://make.wordpress.org/accessibility/handbook/)
WordPress accessibility handbook.

**UMD Accessibility References**
[http://www.d.umn.edu/itss/training/online/webdesign/accessibility.html](http://www.d.umn.edu/itss/training/online/webdesign/accessibility.html)
University of Minnesota Duluth huge list of accessibility resources.

**WCAG 2.1 checklists**
[https://www.wuhcag.com/wcag-checklist/](https://www.wuhcag.com/wcag-checklist/)
WCAG 2.1 checklists for web developers.

**IBM Accessibility Checklist**
[https://www.ibm.com/able/guidelines/ci162/accessibility\_checklist.html](https://www.ibm.com/able/guidelines/ci162/accessibility_checklist.html)
IBM's WCAG 2.1 compliant checklist.


## Code Inspection & Validation Tools

**Wave WebAIM**
[http://wave.webaim.org/](http://wave.webaim.org/)
Web accessibility audit tool (also a browser extension with document inspection features).

**aXe**
[https://www.deque.com/products/axe/](https://www.deque.com/products/axe/)
Open-source accessibility testing tool that runs in your web browser.

**Tenon**
[https://tenon.io](https://tenon.io/)
Accessibility testing tool, check by URL or code.

**AChecker**
[https://achecker.ca/checker/index.php](https://achecker.ca/checker/index.php)
Web accessibility reporting tool for HTML.

**Google Accessibility Tools**
[https://chrome.google.com/webstore/detail/accessibility-developer-t/fpkknkljclfencbdbgkenhalefipecmb](https://chrome.google.com/webstore/detail/accessibility-developer-t/fpkknkljclfencbdbgkenhalefipecmb)
Google Chrome extension for running basic accessibility tests from your browser.

**Funkify**
[https://www.funkify.org/](https://www.funkify.org/)
Google Chrome extension disability simulator, see through the eyes of users with disabilities.

**W3C List of Tools**
[https://www.w3.org/WAI/ER/tools/](https://www.w3.org/WAI/ER/tools/)
Comprehensive list of web accessibility evaluation and tools.

**Siteimprove Accessibility Checker**
[https://chrome.google.com/webstore/detail/siteimprove-accessibility/efcfolpjihicnikpmhnmphjhhpiclljc](https://chrome.google.com/webstore/detail/siteimprove-accessibility/efcfolpjihicnikpmhnmphjhhpiclljc)
Google Chrome extension to evaluate any web page for accessibility issues.

**Tingtun Checker**
[http://checkers.eiii.eu/en/pagecheck/](http://checkers.eiii.eu/en/pagecheck/)
European Internet Inclusion Initiative web page accessibility checker.

**a11y.css**
[https://ffoodd.github.io/a11y.css/](https://ffoodd.github.io/a11y.css/)
Firefox & Chrome bookmarklet that warns developers about risks and mistakes in HTML code.


## Screen Reading Tools

**JAWS**
[http://www.freedomscientific.com/Products/Blindness/JAWS](http://www.freedomscientific.com/Products/Blindness/JAWS)
Job Access with Speech (JAWS) is the world's most popular screen reader.

**VoiceOver**
[https://www.apple.com/accessibility/mac/vision/](https://www.apple.com/accessibility/mac/vision/)
Free screen reader for Mac iOS X and OS X.

**Dolphin SuperNova**
[https://yourdolphin.com/](https://yourdolphin.com/)
Screen reader for Windows tablets, laptops and desktops.

**ChromeVox**
[https://chrome.google.com/webstore/detail/chromevox/kgejglhpjiefppelpmljglcjbhoiplfn](https://chrome.google.com/webstore/detail/chromevox/kgejglhpjiefppelpmljglcjbhoiplfn)
Free screen reader for Google Chrome.

**TalkBack**
[https://play.google.com/store/apps/details?id=com.google.android.marvin.talkback](https://play.google.com/store/apps/details?id=com.google.android.marvin.talkback)
Screen reader for Android phones, laptops, and tablets.

**Narrator**
[https://support.microsoft.com/en-us/help/17173/windows-10-hear-text-read-aloud](https://support.microsoft.com/en-us/help/17173/windows-10-hear-text-read-aloud)
Screen reader built-in to Windows 10.

**WebAnywhere**
[https://webinsight.cs.washington.edu/wa/](https://webinsight.cs.washington.edu/wa/)
Free web-based screen reader for all operating systems and web browsers.

**Window-Eyes**
[https://www.gwmicro.com/Window-Eyes/](https://www.gwmicro.com/Window-Eyes/)
Trusted screen reader for Windows.

**NVDA**
[https://www.nvaccess.org/](https://www.nvaccess.org/)
Free screen reader for Windows.


## Color Contrast Tools

**Accessible Colours**
[http://accessible-colors.com/](http://accessible-colors.com/)
WCAG 2.1 AA and AAA color contrast checker.

**WebAIM Contrast Checker**
[https://webaim.org/resources/contrastchecker/](https://webaim.org/resources/contrastchecker/)
Contrast checker to evaluate links that are identified using color alone.

**Contrast Checker**
[https://contrastchecker.com/](https://contrastchecker.com/)
Web Content Accessibility Guidelines WCAG color contrast checker.

**Color Safe**
[http://colorsafe.co/](http://colorsafe.co/)
Color palettes based on WCAG Guidelines of text and background contrast ratios.

**Colour Contrast Analyser**
[https://developer.paciellogroup.com/resources/contrastanalyser/](https://developer.paciellogroup.com/resources/contrastanalyser/)
Colour Contrast Analyser (CCA) to determine the legibility of text and the contrast of graphics.

**Are My Buttons Accessible?**
[https://www.aremycoloursaccessible.com/](https://www.aremycoloursaccessible.com/)
A tool to make buttons and color combinations WCAG 2.1 compatible.

**Button Contrast Checker**
[https://www.aditus.io/button-contrast-checker/](https://www.aditus.io/button-contrast-checker/)
Check if buttons have enough contrast and are compliant with WCAG 2.1.

**Accessible color palette builder**
[https://toolness.github.io/accessible-color-matrix/](https://toolness.github.io/accessible-color-matrix/)
WCAG 2.1 AA and AAA color palettes contrast checker.

**Color Contrast Analyzer**
[https://chrome.google.com/webstore/detail/color-contrast-analyzer/dagdlcijhfbmgkjokkjicnnfimlebcll](https://chrome.google.com/webstore/detail/color-contrast-analyzer/dagdlcijhfbmgkjokkjicnnfimlebcll)
Google Chrome extension that analyzes a web page for conformance with WCAG 2.1.

**Colour Contrast Analyser**
[https://www.visionaustralia.org/services/digital-access/resources/colour-contrast-analyser](https://www.visionaustralia.org/services/digital-access/resources/colour-contrast-analyser)
Vision Australia's color contrast analyzer is compatible with WCAG 2.1.


## PDF & Word Accessibility Tools

**PAVE**
[http://pave-pdf.org/](http://pave-pdf.org/)
A free tool that makes your PDF documents accessible.

**Tingtun Checker**
[http://checkers.eiii.eu/en/pdfcheck/](http://checkers.eiii.eu/en/pdfcheck/)
European Internet Inclusion Initiative pdf document accessibility checker.

**Accessibility Now**
[https://www.accessibilitynow.com/](https://www.accessibilitynow.com/)
Upload PDF files and get a properly formatted, compliant accessible PDF document back.

**WebAIM PDF Accessibility**
[https://webaim.org/techniques/acrobat/acrobat](https://webaim.org/techniques/acrobat/acrobat)
Guide to accessibility features and best practices in Acrobat DC and XI.

**PAC 3**
[https://www.access-for-all.ch/en/pdf-lab/pdf-accessibility-checker-pac.html](https://www.access-for-all.ch/en/pdf-lab/pdf-accessibility-checker-pac.html)
Free PDF accessibility checker (PAC 3).

**Adobe**
[https://www.adobe.com/accessibility/pdf/pdf-accessibility-overview.html](https://www.adobe.com/accessibility/pdf/pdf-accessibility-overview.html)
Adobe's guide to making PDF documents accessible.

**Microsoft**
[https://support.office.com/en-us/article/create-accessible-pdfs-064625e0-56ea-4e16-ad71-3aa33bb4b7ed](https://support.office.com/en-us/article/create-accessible-pdfs-064625e0-56ea-4e16-ad71-3aa33bb4b7ed)
Microsoft's guide to making PDF documents accessible.

**CommonLook PDF Validator**
[https://commonlook.com/accessibility-software/pdf-validator/](https://commonlook.com/accessibility-software/pdf-validator/)
Free plugin for testing a PDF document accessibility.

**Section508**
[https://www.section508.gov/create/pdfs](https://www.section508.gov/create/pdfs)
Video guide to making PDF documents accessible.

**Axes4**
[https://www.axes4.com/](https://www.axes4.com/)
Convert Word documents into accessible PDFs.


## Accessibility Companies

Let our accessibility experts at [Labinator.com](https://labinator.com) handle your web project.
We provide expert accessibility web development solutions and templates. Learn more at: [https://labinator.com](https://labinator.com/)